Name:           onedrivectl
Version:        0.2
Release:        1%{?dist}
Summary:        

License:        
URL:            
Source0:        

BuildRequires:  make
Requires:       libnotify zenity

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Thu Apr 08 2021 Daniel J. R. May <daniel.may@kada-media.com>
- 
