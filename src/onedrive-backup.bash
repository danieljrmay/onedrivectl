#!/usr/bin/bash
#
# onedrive-backup.bash
#
# Author: Daniel J. R. May
# Version: 0.2, 08 April 2021
#

readonly ONEDRIVECTL_REMOTE_PATH="OneDrive:"
readonly ONEDRIVECTL_LOCAL_BACKUP_PATH="~/Backups/OneDrive"

# Create the backup destination directory if it does not already exist
if [[ -d "$ONEDRIVECTL_LOCAL_BACKUP_PATH" ]]
then
    # Backup directory already exists, so we can continue
    :
else
    echo "Directory $ONEDRIVECTL_LOCAL_BACKUP_PATH does not exist, so we will attempt to create it."
    if mkdir -p $ONEDRIVECTL_LOCAL_BACKUP_PATH
    then
        echo "Done"
    else
        echo "ERROR: Failed to create directory $ONEDRIVECTL_LOCAL_BACKUP_PATH"
        exit 1
    fi
fi

# Run the backup
rclone sync --progress "$ONEDRIVECTL_REMOTE_PATH" "$ONEDRIVECTL_LOCAL_BACKUP_PATH"
