#!/usr/bin/bash
#
# install.bash
#
# Author: Daniel J. R. May
# Version: 0.1, 26 November 2020
#
# Install the onedrive-* scripts and services.

if [[ $(whoami) != 'root' ]]; then
	echo "Only root can run this script."
	exit 1
fi

: "${LOCAL_USER:=local.user}"
: "${ONEDRIVE_USER:=onedrive.user}"

readonly bindir=/usr/bin
readonly docdir=/usr/share/doc/onedrive
readonly systemd_user_dir=/home/${LOCAL_USER}/.config/systemd/user

install onedrive-ask-backup.bash $bindir/onedrive-ask-backup
install onedrive-backup.bash $bindir/onedrive-backup
install onedrive-backup-dry-run.bash $bindir/onedrive-backup-dry-run
install onedrive-mount.bash $bindir/onedrive-mount
install onedrive-unmount.bash $bindir/onedrive-unmount
install --directory --owner=${LOCAL_USER} --group=${LOCAL_USER} $systemd_user_dir
install --mode=644 --owner=${LOCAL_USER} --group=${LOCAL_USER} --target-directory=$systemd_user_dir -- *.service *.timer
install -D --mode=644 --target-directory=$docdir README.org
