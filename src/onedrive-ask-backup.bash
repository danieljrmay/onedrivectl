#!/usr/bin/bash
#
# onedrive-ask-backup.bash
#
# Author: Daniel J. R. May
# Version: 0.2, 08 April 2021
#

readonly ONEDRIVECTL_LOCAL_BACKUP_PATH=~/Backups/OneDrive

question_text="<big><b>Do you want to backup your OneDrive?</b></big>\n\n"
question_text+='Only answer “Yes” if you know that your OneDrive files are in good order. '
question_text+='Answer “No” if some of your OneDrive files have been lost, deleted by mistake or compromised in any way.'

if ! zenity --question --default-cancel --text "$question_text" --width=600
then
    exit 0
fi

if /usr/bin/onedrive-backup
then
    notify-send --urgency=normal \
                --icon='dialog-information' \
                --category='transfer.complete' \
                'OneDrive Backup Complete' \
                "Successfully backed up to $ONEDRIVECTL_LOCAL_BACKUP_PATH."
else
    notify-send --urgency=critical \
                --icon='dialog-error' \
                --category='transfer.error' \
                'OneDrive Backup Failed' \
                'Check your network connection, and try again by typing “onedrive-backup” at the command line.'
    exit 1
fi

