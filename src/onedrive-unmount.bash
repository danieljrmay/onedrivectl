#!/usr/bin/bash
#
# onedrive-unmount.bash
#
# Author: Daniel J. R. May
# Version: 0.2, 08 April 2021
#

readonly ONEDRIVECTL_LOCAL_BACKUP_PATH="~/Backups/OneDrive"

# Unmount the OneDrive
fusermount -u "$ONEDRIVECTL_LOCAL_BACKUP_PATH"
