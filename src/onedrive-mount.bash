#!/usr/bin/bash
#
# onedrive-mount.bash
#
# Author: Daniel J. R. May
# Version: 0.2,08 April 2021
#

readonly ONEDRIVECTL_REMOTE_PATH="OneDrive:"
readonly ONEDRIVECTL_LOCAL_MOUNT_PATH="~/OneDrive"

# Create the local mount directory if it does not already exist
if [[ -d "$ONEDRIVECTL_LOCAL_MOUNT_PATH" ]]
then
    # Local mount directory already exists, so we can continue
    :
else
    echo "Directory $ONEDRIVECTL_LOCAL_MOUNT_PATH does not exist, so we will attempt to create it."
    if mkdir -p $ONEDRIVECTL_LOCAL_MOUNT_PATH
    then
        echo "Done"
    else
        echo "ERROR: Failed to create directory $ONEDRIVECTL_LOCAL_MOUNT_PATH"
        exit 1
    fi
fi

# Mount the OneDrive
rclone mount "$ONEDRIVECTL_REMOTE_PATH" "$ONEDRIVECTL_LOCAL_MOUNT_PATH" --vfs-cache-mode writes &
