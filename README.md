# onedrivectl

A utility to allow the automatic & manual mounting and local synchronization of Microsoft OneDrive cloud-based storage with Linux clients.